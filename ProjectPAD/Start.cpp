#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int addPhoto(string name) {
	VideoCapture cap;

	if (!cap.open(0)) {
		cout << "error reading from the camera" << endl;
		return -1;
	}
	else {
		Mat img;
		CascadeClassifier face_cascade;
		if (!face_cascade.load("haar/haarcascade_frontalface_alt2.xml")) {
			cout << "error loading cascade file" << endl;
			return 20;
		}

		for (;;)
		{
			cap >> img;
			cap.retrieve(img, CAP_OPENNI_BGR_IMAGE);

			vector<Rect> faces;

			face_cascade.detectMultiScale(img, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(140, 140));

			char key = (char)waitKey(20);

			for (int i = 0; i < faces.size(); i++)
			{
				Mat head;
				Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);
				if (key == 's') {
					head = img(faces[i]);
					string path = "c:/ProjectPAD/ProjectPAD/Dataset/" + name;
					string ext = ".jpg";
					imwrite(path + ext, head);
					imshow(name, head);
					cout << "face is at position:\n X: "<<faces[i].x << "\n Y: "<<faces[i].y << endl;
				}
				else  if (key == 'q') {
					cout << "quit\n";
					return 100;
				}

				ellipse(img, center, Size(faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar(255, 0, 0), 4, 8, 0);
				if(faces[i].x < 100||faces[i].x>400||faces[i].y<60||faces[i].y>260) {
					ellipse(img, center, Size(faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar(0, 0, 255), 4, 8, 0);
				}
			}

			imshow("live stream", img);
		}
	}
	return 0;
}

int main(int argc, char** argv)
{
	string name;
	cout << "enter name:\n";
	cin >> name;

	addPhoto(name);	
	return 0;
}
